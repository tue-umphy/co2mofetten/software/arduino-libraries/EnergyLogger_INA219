/*
  EnergyLogger_INA219.cpp - Library for integrating energy with INA219
  Created by Yann Büchau, April 23, 2018.

  This bases on the Adafruit_INA219 library
*/

#include <EnergyLogger_INA219.h>
#include <Adafruit_INA219.h>
#include <Arduino.h>
#include <Time.h>

#ifdef EnergyLogger_INA219_DEBUG
#define D(x) Serial.print("EnergyLogger_INA219: ");Serial.println(x);Serial.flush();
#define D2(x,y) Serial.print("EnergyLogger_INA219: ");Serial.print(x);Serial.println(y);Serial.flush();
#else
#define D(x)
#define D2(x,y)
#endif

/* initialize energy logging, i.e. reset all values */
void EnergyLogger_INA219::init_energy_logging() {
  total_recorded_millis = 0;
  millis_last_measurement = millis();
  last_power_mW = 0;
  total_energy_J = 0;
  }

/* log a measurement */
void EnergyLogger_INA219::log_energy() {
  float current_power_mW = 0;
  long millis_this_measurement = 0;
  int millis_diff = 0;
  float energy_since_last_measurement_J = 0;
  millis_this_measurement = millis();
  current_power_mW = getPower_mW(); // measure power
  D2("Current power [mW] ", current_power_mW);
  D2("Last power [mW] ", last_power_mW);
  D2("Current millis [ms] ", millis_this_measurement);
  D2("Last millis [ms] ", millis_last_measurement);
  millis_diff = millis_this_measurement - millis_last_measurement;
  D2("time since last measurement [ms] ", millis_diff);
  energy_since_last_measurement_J = integrate( // integrate energy cosumption
    millis_last_measurement, millis_this_measurement, // time
    last_power_mW, current_power_mW  // power
    ) * 1e-6; // convert to Joule
  D2("energy since last measurement [J] ", energy_since_last_measurement_J);
  total_energy_J += energy_since_last_measurement_J;
  D2("total energy since init [J] ", total_energy_J);
  total_energy_Wh = total_energy_J / 3600;
  last_power_mW = current_power_mW; // save last power measurement
  millis_last_measurement = millis_this_measurement; // save last time
  total_recorded_millis += millis_diff;
  D2("average power consumption [mW] ", average_power_mW());
}

/* integrate graph between (x1,y1) and (x2,y2) with trapezoid rule */
float EnergyLogger_INA219::integrate(int x1, int x2, float y1, float y2) {
  return (float)((unsigned long)(x2 - x1) * (y2 + y1)) / 2.0;
}

/* average energy consumption */
float EnergyLogger_INA219::average_power_mW() {
  return total_energy_J / (float)(total_recorded_millis) * 1e6;
}
