#ifndef EnergyLogger_INA219_h
#define EnergyLogger_INA219_h

#include <Adafruit_INA219.h>

class EnergyLogger_INA219: public Adafruit_INA219
{
  using Adafruit_INA219::Adafruit_INA219;
  public:
    float total_energy_J;
    float total_energy_Wh;
    void log_energy();
    void init_energy_logging();
    static float integrate(int, int, float, float);
    float average_power_mW();
  protected:
    float last_power_mW;
    unsigned long millis_last_measurement;
    unsigned long total_recorded_millis;
};

#endif
