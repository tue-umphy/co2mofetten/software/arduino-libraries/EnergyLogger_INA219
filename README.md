# INA219_EnergyLogger Arduino library

This library provides a class to integrate the energy usage using an INA219.

## Requirements

This library requires the [Adafruit_INA219
library](https://github.com/adafruit/Adafruit_INA219).

## Capabilities

- ...
